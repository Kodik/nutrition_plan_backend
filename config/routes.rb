Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  post 'sign_up', to: 'users#sign_up'
  post 'sign_in', to: 'users#sign_in'
  delete 'sign_out', to: 'users#sign_out'
  get 'user_days', to: 'users#user_days'

  resources :days, only: %i[index show create destroy]
  resources :ingredients, only: %i[index]
end
