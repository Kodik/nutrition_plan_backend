# README

## Dependencies Backend

- Ruby 2.6.5

## Database

- PostgreSQL

## How to start app first time?

1. `bundle install`
2. `rake db:create`
3. `rake db:migrate`
4. `rails s`
