# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user = User.create(name: 'Example', email: 'example@example.com', password: 'Zaqw123')
ingredient = Ingredient.create(name: 'Rice', kcal: 400, carbohydrates: 54, protein: 6, fat: 2)
day = Day.create(user_id: user.id, name: 0)
meal = Meal.create(day_id: day.id, name: 'Rice meal')
meal.ingredients << ingredient
