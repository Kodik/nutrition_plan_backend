class CreateIngredientsMeals < ActiveRecord::Migration[6.0]
  def change
    create_table :ingredients_meals, id: false do |t|
      t.references :ingredient, foreign_key: true
      t.references :meal, foreign_key: true
    end
  end
end
