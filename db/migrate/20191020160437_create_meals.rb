class CreateMeals < ActiveRecord::Migration[6.0]
  def change
    create_table :meals do |t|
      t.references :day, foreign_key: true
      t.string :name, null: false

      t.timestamps
    end
  end
end
