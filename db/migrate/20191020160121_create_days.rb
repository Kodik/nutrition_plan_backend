class CreateDays < ActiveRecord::Migration[6.0]
  def change
    create_table :days do |t|
      t.references :user, foreign_key: true
      t.integer :name

      t.timestamps
    end
  end
end
