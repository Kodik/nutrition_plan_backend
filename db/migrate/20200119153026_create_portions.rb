class CreatePortions < ActiveRecord::Migration[6.0]
  def change
    create_table :portions do |t|
      t.references :meal, foreign_key: true
      t.references :ingredient, foreign_key: true
      t.integer :grams
      t.integer :kcal, default: 0
      t.float :carbohydrates, default: 0
      t.float :protein, default: 0
      t.float :fat, default: 0

      t.timestamps
    end
  end
end
