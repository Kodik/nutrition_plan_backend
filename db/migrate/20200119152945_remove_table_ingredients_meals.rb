class RemoveTableIngredientsMeals < ActiveRecord::Migration[6.0]
  def change
    drop_table :ingredients_meals
  end
end
