class ChangeIntegersToFloats < ActiveRecord::Migration[6.0]
  def change
    change_column :ingredients, :carbohydrates, :float
    change_column :ingredients, :protein, :float
    change_column :ingredients, :fat, :float
  end
end
