class PortionSerializer < ActiveModel::Serializer
  attributes :id, :grams, :kcal, :carbohydrates, :protein, :fat, :meal_id
  has_many :ingredients
end
