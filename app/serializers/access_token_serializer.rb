class AccessTokenSerializer < ActiveModel::Serializer::ErrorSerializer
  attributes :id, :token
end
