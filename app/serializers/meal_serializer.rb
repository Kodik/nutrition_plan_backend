class MealSerializer < ActiveModel::Serializer
  attributes :id, :name, :day_id
  has_many :portions, each_serializer: PortionSerializer
end
