class IngredientSerializer < ActiveModel::Serializer
  attributes :id, :name, :kcal, :carbohydrates, :protein, :fat
end
