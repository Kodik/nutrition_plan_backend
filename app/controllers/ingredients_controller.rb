class IngredientsController < ApplicationController
  def index
    ingredients = Ingredient.all
    render json: ingredients, each_serializer: IngredientSerializer, status: :ok
  end

  def create
    raise AuthorizationError unless current_user.admin?
    ingredient = Ingredient.new(ingredient_params)
    ingredient.save!
  rescue ActiveRecord::RecordInvalid
    render(
      json: ingredient,
      adapter: :json_api,
      serializer: ErrorSerializer,
      status: :unprocessable_entity
    )
  end

  def update
    raise AuthorizationError unless current_user.admin?
    ingredient = Ingredient.find(params[:id])
    ingredient.update_attributes!(ingredient_params)
  rescue ActiveRecord::RecordInvalid
    render(
      json: ingredient,
      adapter: :json_api,
      serializer: ErrorSerializer,
      status: :unprocessable_entity
    )
  end

  def destroy
    raise AuthorizationError unless current_user.admin?
    ingredient = Ingredient.find(params[:id])
    ingredient.destroy!
    head :no_content
  rescue ActiveRecord::RecordNotFound
    authorization_error
  end

  private

  def ingredient_params
    params.require(:data).require(:attributes).
      permit(:name, :kcal, :carbohydrates, :protein, :fat) ||
      ApplicationController::Parameters.new
  end
end
