class DaysController < ApplicationController
  def index
    days = current_user.days
    render json: days, status: :ok
  end

  def show
    day = current_user.days.find(params[:id])
    render json: day, status: :ok
  rescue ActiveRecord::RecordNotFound
    authorization_error
  end

  def create
    day = Day.new(day_params)
    day.user_id = current_user.id
    day.save!
    render json: day, status: :created
  rescue ActiveRecord::RecordInvalid
    render(
      json: day,
      adapter: :json_api,
      serializer: ErrorSerializer,
      status: :unprocessable_entity
    )
  end

  def destroy
    day = current_user.days.find(params[:id])
    day.destroy!
    head :no_content
  rescue ActiveRecord::RecordNotFound
    authorization_error
  end

  private

  def day_params
    params.require(:data).require(:attributes).permit(:name) ||
      ApplicationController::Parameters.new
  end
end
