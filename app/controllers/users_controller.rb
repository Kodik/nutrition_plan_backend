class UsersController < ApplicationController
  skip_before_action :authorize!, only: %i[sign_in sign_up]

  def sign_in
    user = User.find_by_email(sign_in_params[:email])
    raise AuthenticationError unless user
    raise AuthenticationError unless user.password == sign_in_params[:password]

    access_token =
      if user.access_token.present?
        user.access_token
      else
        user.create_access_token
      end

    render json: access_token, status: :created
  end

  def sign_up
    user = User.new(sign_up_params)
    user.save!
    Day.names.each { |k, _v| Day.create!(name: k, user_id: user.id) }
    render json: user, status: :created
  rescue ActiveRecord::RecordInvalid
    render(
      json: user,
      adapter: :json_api,
      serializer: ErrorSerializer,
      status: :unprocessable_entity
    )
  end

  def sign_out
    current_user.access_token.destroy!
    head :no_content
  end

  def user_days
    render json: current_user.days.includes(meals: { portions: :ingredient }).to_json(
      include: { meals: { include: { portions: { include: :ingredient } } } }
    ), status: :ok
  end

  private

  def sign_in_params
    params.require(:data).require(:attributes).permit(:email, :password) ||
      ApplicationController::Parameters.new
  end

  def sign_up_params
    params.require(:data).require(:attributes).permit(:email, :name, :password) ||
      ApplicationController::Parameters.new
  end
end
