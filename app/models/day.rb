class Day < ApplicationRecord
  belongs_to :user
  has_many :meals, dependent: :destroy

  enum name: %i[
    monday
    tuesday
    wednesday
    thursday
    friday
    saturday
    sunday
  ]

  validates :name, presence: true, inclusion: { in: Day.names.keys }
end
