class Meal < ApplicationRecord
  has_many :portions
  has_many :ingredients, through: :portions
  belongs_to :day

  validates :name, presence: true
end
