class User < ApplicationRecord
  include BCrypt

  has_many :days
  has_one :access_token, dependent: :destroy

  # Validations
  validates :email, presence: true, uniqueness: true, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/ }
  validates :name, presence: true, uniqueness: true
  validate :password_presence

  # Callbacks
  before_validation :email_to_downcase

  def password
    @password ||= Password.new(encrypted_password)
  end

  def password=(new_password)
    @password = Password.create(new_password)
    self.encrypted_password = @password
  end

  private

  def email_to_downcase
    self.email = self.email&.downcase
  end

  def password_presence
    errors.add(:password, "can't be blank") if encrypted_password.nil?
  end
end
