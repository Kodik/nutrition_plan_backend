class Ingredient < ApplicationRecord
  has_many :portions
  has_many :meals, through: :portions

  validates :name, presence: true
  validates :kcal, presence: true, inclusion: { in: 0..900 }
  validates :carbohydrates, presence: true, inclusion: { in: 0..100 }
  validates :protein, presence: true, inclusion: { in: 0..100 }
  validates :fat, presence: true, inclusion: { in: 0..100 }
end
