class Portion < ApplicationRecord
  belongs_to :ingredient
  belongs_to :meal

  validates :grams, presence: true

  before_save :calculate_kcal

  private

  def calculate_kcal
    self.kcal = ingredient.kcal * grams / 100
    self.carbohydrates = ingredient.carbohydrates * grams / 100
    self.protein = ingredient.protein * grams / 100
    self.fat = ingredient.fat * grams / 100
  end
end
