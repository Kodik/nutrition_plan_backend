require 'csv'

namespace :import do
  desc "This task imports ingredients from CSV file to a database"
  task ingredients: :environment do
    file_path = "#{Rails.root}/public/ingredients.csv"

    CSV.foreach(file_path, col_sep: ';', quote_char: '"', headers: true) do |row|
      ingredient = Ingredient.find_by_name(row['name'])
      if ingredient.present?
        ingredient.update_attributes!(
          kcal: row['kcal'].to_i,
          carbohydrates: row['carbo'].to_f,
          protein: row['protein'].to_f,
          fat: row['fat'].to_f
        )
      else
        Ingredient.create!(
          name: row['name'],
          kcal: row['kcal'].to_i,
          carbohydrates: row['carbo'].to_f,
          protein: row['protein'].to_f,
          fat: row['fat'].to_f
        )
      end
    end
  end
end
